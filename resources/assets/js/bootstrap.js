let Angular = require("angular");
let AngularRoute = require("angular-route");
let AngularPagination = require("angular-utils-pagination");

let Sweetalert = require("sweetalert");

let Routes = require("./config/routes");
let Run = require("./config/run");

let ServiceUsers = require("./services/serviceUsers");

let FactoryUsers = require("./factories/factoryUsers");

let ControllerApp = require("./controllers/controllerApp");
let ControllerCheckeds = require("./controllers/controllerCheckeds");
let ControllerDeletes = require("./controllers/controllerDeletes");
let ControllerUserDetails = require("./controllers/controllerUserDetails");
let ControllerUsers = require("./controllers/controllerUsers");

let ModuleApp = angular.module("module_app", [
    "ngRoute",
	"angularUtils.directives.dirPagination"
]);

Routes.$inject = ["$routeProvider"];
ModuleApp.config(Routes);

Run.$inject = ["$rootScope", "$window", "FactoryUsers"];
ModuleApp.run(Run);

ServiceUsers.$inject = ["$http"];
ModuleApp.service("ServiceUsers", ServiceUsers);

FactoryUsers.$inject = ["$rootScope", "$log", "$q", "ServiceUsers"];
ModuleApp.factory("FactoryUsers", FactoryUsers);

ControllerApp.$inject = ["$scope", "$route", "FactoryUsers"];
ModuleApp.controller("ControllerApp", ControllerApp);

ControllerCheckeds.$inject = ["$scope", "$route", "FactoryUsers"];
ModuleApp.controller("ControllerCheckeds", ControllerCheckeds);

ControllerDeletes.$inject = ["$scope", "$route", "FactoryUsers"];
ModuleApp.controller("ControllerDeletes", ControllerDeletes);

ControllerUserDetails.$inject = ["$scope", "$route", "$location", "FactoryUsers"];
ModuleApp.controller("ControllerUserDetails", ControllerUserDetails);

ControllerUsers.$inject = ["$scope", "$route", "FactoryUsers"];
ModuleApp.controller("ControllerUsers", ControllerUsers);