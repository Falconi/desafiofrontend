module.exports = function($http){
	return {
		GET_list: function(){
			return $http.get("https://randomuser.me/api/?results=25&nat=br&inc=name,location,email,registered,cell,id,picture,login");
		}
	};	
}