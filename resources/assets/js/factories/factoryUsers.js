module.exports = function($rootScope, $log, $q, ServiceUsers){
	
	class User {
		
		constructor(id, image, name, email, number, location, password, birth){
			this.id = id;
			this.image = image;
			this.name = name;
			this.email = email;
			this.number = number;
			this.location = location;
			this.password = password;
			this.birth = birth;
		}
		
	}
	
	let USERS = {
		all: new Array(),
		uncheckeds: new Array(),
		checkeds: new Array(),
		deletes: new Array(),
		start: {
			status: false,
			init: () => {
				USERS.uncheckeds = USERS.all.slice();
				USERS.start.status = true;
			}
		}
	}
	
	function apiValidate(response){
		response.forEach((element, index) => {
			element.registered.date = element.registered.date.split('T')[0];
			USERS.all.push(new User(
				(index + 1),
				element.picture.large,
				element.name.first,
				element.email,
				element.cell,
				element.location.street.trim() + ', ' + element.location.city + ' - ' + element.location.state,
				element.login.password,
				element.registered.date.split('-').reverse()[0] + '/' +
				element.registered.date.split('-').reverse()[1] + '/' +
				element.registered.date.split('-').reverse()[2]
			));	
		});
		
		/* 
		Imagem de perfil aleatória 
		*/
		let userId = Math.round(Math.random(1, 10) * USERS.all.length);
		// Caso arredondar pra zero... O valor userId passará a ser 1.
		if(userId == 0){
			userId = 1;
		}
		let user = USERS.all.filter(element => element.id == userId)[0];
		$rootScope.imagePerfil = user.image;
		/* -- // -- */
		
		USERS.start.init();
		
		return USERS.all;
	}
	
	return{

		getApiStatus: () => {
			return USERS.start.status;
		},
		
		apiInit: () => {
			let callback = $q.defer();
			
			if(USERS.start.status){
				callback.resolve(USERS.uncheckeds);
			}else{
				ServiceUsers.GET_list().then(response => {
					if(response.data.results){
						callback.resolve(apiValidate(response.data.results));
						$log.info("ServiceUsers: OK!");
					}else{
						callback.reject(false);
						$log.warn("ServiceUsers: Não há itens cadastrados");
					}
				},
				() => {
					callback.reject(false);
					$log.error("ServiceUsers: Não foi possível acessar as informações");
				});
			}
			
			return callback.promise;
		},
		
		getListUncheckeds: () => {
			return USERS.uncheckeds;
		},
		
		getListCheckeds: () => {
			return USERS.checkeds;
		},
		
		getListDeletes: () => {
			return USERS.deletes;
		},
		
		getUser: userId => {
			let callback = $q.defer();
			
			if(USERS.start.status){
				callback.resolve(USERS.all.filter(element => userId == element.id)[0]);
			}else{
				ServiceUsers.GET_list().then(response => {
					if(response.data.results){
						let users = apiValidate(response.data.results);
						callback.resolve(users.filter(element => userId == element.id)[0]);
						$log.info("ServiceUsers: OK!");
					}else{
						callback.reject(false);
						$log.warn("ServiceUsers: Não há itens cadastrados");
					}
				},
				() => {
					callback.reject(false);
					$log.error("ServiceUsers: Não foi possível acessar as informações");
				});				
			}
			
			return callback.promise;
		},
		
		setDelete: (userId, array) => {
			let position;
			array.forEach((element, indice) => {
				if(element.id == userId){
					USERS.deletes.push(array.splice(indice, 1)[0]);
					return false;
				}
			});
		},

		setChecked: (userId, array) => {
			let position;
			array.forEach((element, indice) => {
				if(element.id == userId){
					USERS.checkeds.push(array.splice(indice, 1)[0]);
					return false;
				}
			});
		},

		setUnchecked: (userId, array) => {
			let position;
			array.forEach((element, indice) => {
				if(element.id == userId){
					USERS.uncheckeds.push(array.splice(indice, 1)[0]);
					return false;
				}
			});
		}
	};
}