module.exports = function ($routeProvider) {
	
	// $locationProvider.html5Mode(true);
	
	let sufix = " | Desafio Frontend";
	$routeProvider.when("/", {
		templateUrl: "html/pages/listing-all.html",
		controller: "ControllerUsers",
		title: "Todos" + sufix
	})
	
	.when("/visualizados", {
		templateUrl: "html/pages/listing-checkeds.html",
		controller: "ControllerCheckeds",
		title: "Visualizados" + sufix
	})
	
	.when("/deletados", {
		templateUrl: "html/pages/listing-deletes.html",
		controller: "ControllerDeletes",
		title: "Deletados" + sufix
	})
	
	.when("/usuario/:id", {
		templateUrl: "html/pages/user-details.html",
		controller: "ControllerUserDetails",
		title: "Usuário" + sufix
	})
	
	$routeProvider.otherwise({redirectTo: "/"});
};