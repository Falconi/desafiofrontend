module.exports = function($rootScope, $window, FactoryUsers){
	
	$rootScope.$on( "$routeChangeStart", function(event, next, current) {
		/* 	Aplicando o title da página e armazenando a página atual / anterior */
		document.querySelector("html head title").innerHTML = next.$$route.title;
		$rootScope.pageCurrent = next.$$route.originalPath;
		$rootScope.pagePrev = () => {
			return $window.history.back();
		}
	});

}