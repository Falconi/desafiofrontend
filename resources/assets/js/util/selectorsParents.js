class UTIL {

    static getParentByClass (element, classe){
        let parent;
        if(element && classe){
            while(element.parentNode){
                if(element.parentNode.classList && element.parentNode.classList.contains(classe)){
                    parent = element.parentNode;
                    break;
                }
                element = element.parentNode;
            }
        }
        return parent;
    }

    static getParentByTagName (element, tag){
        let parent;
        if(element && tag){
            while(element.parentNode){
                if(element.parentNode.tagName.toLowerCase() == tag){
                    parent = element.parentNode;
                    break;
                }
                element = element.parentNode;
            }
        }
        return parent;
    }
}

module.exports = UTIL;