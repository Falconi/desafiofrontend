module.exports = function($scope, $route, FactoryUsers){
	
	/*
	Aplicando o valor do campo search no scope
	---------------------------------------
	*/
	let inputSearch = document.querySelector('#input-search');
	$scope.search = inputSearch.value;
	inputSearch.addEventListener('keyup', function(){
		$scope.$apply(() => {
			$scope.search = this.value;
		});
	});
	/* -- // -- */	
	
	// Se ainda não foi iniciado a api, inicia
	if(!FactoryUsers.getApiStatus()){
		FactoryUsers.apiInit();
	}

	$scope.usersCheckeds = FactoryUsers.getListCheckeds();
	
	$scope.userUnchecked = userId => {
		FactoryUsers.setUnchecked(userId, FactoryUsers.getListCheckeds());
		$scope.usersCheckeds = FactoryUsers.getListCheckeds();
	}

	$scope.userDelete = userId => {
		swal({
			title: 'Excluir',
			text: 'Tem certeza que deseja excluir o(a) usuário(a) ?',
			icon: '/assets/img/icon-alert-warning.svg',
			className: 'confirm',
			closeOnClickOutside: false,
			closeOnEsc: false,
			buttons: {
				cancel: 'Não',
				confirm: 'Sim' 
			} 
		}).then(value => {
			if(value){
				swal({ 
					title: 'Excluído com sucesso!',
					text: 'Não se preocupe, ficará armazenado na lixeira...',
					icon: '/assets/img/icon-alert-success.svg',
					className: 'success'
				}).then(function(){
					FactoryUsers.setDelete(userId, FactoryUsers.getListCheckeds());
					$scope.$apply(() => {
						$scope.usersAll = FactoryUsers.getListUncheckeds();
					});					
				});
			}
		}, () => {
			swal({ 
				title: 'Erro',
				text: 'Não foi possível excluir, entre em contato com a central.',
				icon: '/assets/img/icon-alert-error.svg',
				className: 'error'
			});
		});		
	}
}