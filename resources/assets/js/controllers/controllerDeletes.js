module.exports = function($scope, $route, FactoryUsers){
	
	/*
	Aplicando o valor do campo search no scope
	---------------------------------------
	*/
	let inputSearch = document.querySelector('#input-search');
	$scope.search = inputSearch.value;
	inputSearch.addEventListener('keyup', function(){
		$scope.$apply(() => {
			$scope.search = this.value;
		});
	});
	/* -- // -- */

	// Se ainda não foi iniciado a api, inicia
	if(!FactoryUsers.getApiStatus()){
		FactoryUsers.apiInit();
	}
	
	$scope.usersDeletes = FactoryUsers.getListDeletes();

	$scope.userChecked = userId => {
		FactoryUsers.setChecked(userId, FactoryUsers.getListDeletes());
		$scope.usersDeletes = FactoryUsers.getListDeletes();
	}
	
	$scope.userUnchecked = userId => {
		FactoryUsers.setUnchecked(userId, FactoryUsers.getListDeletes());
		$scope.usersDeletes = FactoryUsers.getListDeletes();
	}
}