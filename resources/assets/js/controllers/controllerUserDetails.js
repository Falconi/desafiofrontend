const UTIL = require("./../util/selectorsParents");

module.exports = function($scope, $route, $location, FactoryUsers){
    
    let answerList = new Array();
    
    FactoryUsers.getUser($route.current.params.id).then(response => {
        if(response){
            answerList = [
                {
                    label: "name",
                    text: "Olá, meu nome é",
                    value: response.name
                },
                {
                    label: "email",
                    text: "Meu email é",
                    value: response.email
                },
                {
                    label: "birth",
                    text: "Meu aniversário é",
                    value: response.birth
                },
                {
                    label: "location",
                    text: "Meu endereço é",
                    value: response.location
                },
                {
                    label: "number",
                    text: "Meu número é",
                    value: response.number
                },
                {
                    label: "password",
                    text: "Minha senha é",
                    value: response.password
                }
            ];
            
            // Iniciando os campos
            $scope.user = response;
            $scope.answer = {
                text: answerList.filter(element => element.label == "name")[0].text,
                value: response.name
            };
        }else{
            // Não existe o usuário
            $location.path("/");
        }
    });

    $scope.setDetail = (event, answerLabel) => {
        let answer = answerList.find(element => element.label == answerLabel);
        $scope.answer.text = answer.text;
        $scope.answer.value = answer.value;
        
        document.querySelectorAll('.modal-action').forEach(element => element.classList.remove('active'));
        let input = UTIL.getParentByClass(event.target, "modal-action");
        input.classList.add('active');
    }
}