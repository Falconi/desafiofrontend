module.exports = function($scope, $route, FactoryUsers){
	
	/*
	Iniciando o menu mobile
	-------------------------------------------------------------------------
	*/
	if(window.innerWidth < 768){
		$scope.menuStatus = () => $scope.menuShow = !$scope.menuShow;
	}
	/* -- // -- */
	
	/*
	Iniciando o submit do form (retira o focus do input search no mobile)
	-------------------------------------------------------------------------
	*/
	$scope.searchAction = () => {
		document.querySelector('#input-search').blur();
	};
	/* -- // -- */
};