Instalando o projeto
--------------------

1 - Necessário ter o pacote Node e Npm instalado.<br>
2 - Rodar o comando na raiz do projeto “npm install” para instalar as dependências.<br>
3 - Instalar o eslint global… npm install -g eslint (Já está configurado, basta chamar o comando  “eslint ./resources/assets/js/**/*.js” na raiz do projeto. ///---(OPCIONAL)---///<br>
3 - Caso alterar os arquivos scss, js, inserir uma imagem na pasta pictures ou um svg na pasta sprites, é necessário rodar a task usando o comando “npm run build“ ou então abrir um terminal paralelo ao server e rodar o comando "npm run watch", pois este comando fica observando se algum arquivo js/scss é alterado, caso seja ele atualiza os outputs.<br>
4 - Iniciando o server, rode o comando “npm run start”.<br>
5 - Abra o navegador no domínio “http://localhost:8080/.“<br>

OBS: Alterar somente as imagens/sprites e arquivos scss/js na pasta "resources", pois as tasks do webpack joga para a pasta "public" complilando, montando os sprites e copiando as imagens.
<br><br>
Have fun :D
<br><br>
Create by<br>
Leonardo Falconi<br>
11/03/2018<br>